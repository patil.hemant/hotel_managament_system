json.rooms @available_rooms do |room|
  json.(room, :id, :room_number)
end
