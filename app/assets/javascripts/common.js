$(document).ready(function(){
	$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '-3d'
	});

	$('.check').change(function() {
    if ($('.check:checked').length) {
      $('#sub').removeAttr('disabled');
    } else {
      $('#sub').attr('disabled', 'disabled');
    }
	});


  $(".cancel-btn").hover(
      function () {
          $(this).css({"border-color": "#000000"});
      },
      function () {
          $(this).css({"border-color": ""});
      }
);
});