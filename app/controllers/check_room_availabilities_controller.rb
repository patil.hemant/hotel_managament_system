class CheckRoomAvailabilitiesController < ApplicationController
	
	before_action :authenticate_user!

	def new
		@check_room_availability = CheckRoomAvailability.new
	end

	def create
		@check_room_availability = CheckRoomAvailability.new(check_room_availability_params)
		if @check_room_availability.valid?
			redirect_to new_booking_path(:category_id => params[:check_room_availability][:category_id], :check_in_date => params[:check_room_availability][:check_in_date], :check_out_date => params[:check_room_availability][:check_out_date])
		else
			render 'new'
		end
	end

	protected

  def check_room_availability_params
    params.require(:check_room_availability).permit!
  end
end
