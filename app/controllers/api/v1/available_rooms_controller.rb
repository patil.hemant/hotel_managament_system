class Api::V1::AvailableRoomsController < ApplicationController
	
	# below filter is used for token based user authentication(when user is created a random authentication token is generated for that user, which will be used when calling an API)
	before_action :authenticate

	require 'rubygems'
	require 'json'


	def get_available_rooms

		unless params[:check_in_date].present?
			render :status => 401, :json => {:status => "Failure", :message => "Check In is required."}
			return true
		end

		unless params[:check_out_date].present?
			render :status => 401, :json => {:status => "Failure", :message => "Check Out is required."}
			return true
		end

		if params[:room_type].present?
			category = Category.find_by_name(params[:room_type])
			category_id = category.id if category.present?

			unless category.present?
				render :status => 401, :json => {:status => "Failure", :message => "Could not find room with given type."}
				return true
			end		
		end	

		params[:category_id] = category_id
		# here I amhaving class method for Booking class for checking available rooms(common methos is being used for web app and API)
		@available_rooms = Booking.check_for_available_rooms(params)
		# If rooms are not available for given time period the below message is displayed for the user and fi rooms are available then .jbuilder template is rendered
		unless @available_rooms.present?
			render :status => 401, :json => {:status => "Successful", :message => "Sorry! no rooms are available."}
			return true
		end
	end
end
