class BookingsController < ApplicationController
	
  before_action :authenticate_user!

  def index
    # room objects are eager_loaded for query efficiency using 'includes'
    @bookings= current_user.bookings.includes(:room)
  end

  def new
    @available_rooms = Booking.check_for_available_rooms(params)    
  end

  def create
    unless params[:room_ids].present?
      session[:blank_checkboxes] = true
      return redirect_to :back
    end
    booking_ids =[]
    params[:room_ids].each do |room_id|
      booking = Booking.book_room(room_id, params)
      booking_ids << booking.id
    end
    redirect_to thank_you_path(:ids => booking_ids)
  end

  def destroy
    @booking = Booking.find(params[:id])
    @booking.destroy
    redirect_to bookings_path
  end

  protected

  def booking_params
    params.require(:booking).permit!
  end

end
