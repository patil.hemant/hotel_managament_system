class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

def after_sign_in_path_for(resource)
  root_path
end

protected
  def authenticate
    if user_signed_in?
	    user = User.find_by(auth_token: params[:token])
	    unless user.present?
	    	render :status => 404, :json => {:status => "Failure", :message => "Unauthorized User."}
	    end
	  else
	  	render :status => 404, :json => {:status => "Failure", :message => "Unauthorized User."}
	  end  
  end

end
