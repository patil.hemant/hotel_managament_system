class CheckRoomAvailability
  include ActiveModel::Model

  #This model is not database backed, we are not storing any data over here, we have just created this model for validation purposes using ActiveModel Gem

  attr_accessor :category_id, :check_in_date, :check_out_date
  validates :category_id, :presence => { :message => "Please select room category" }
  validates :check_in_date, :presence => { :message => "Please select check-in date" }
  validates :check_out_date, :presence => { :message => "Please select check-out date" }

	validate :booking_till_max_6_months
	validate :check_valid_dates

	def check_valid_dates
		if self.check_in_date.present? and self.check_out_date.present?
			errors.add(:check_out_date, message: "Check-out date can't be less than check-in date")  if self.check_in_date.to_date  > self.check_out_date.to_date
			errors.add(:base, message: "Please enter valid dates")  if self.check_in_date.to_date  < Date.today or self.check_out_date.to_date < Date.today
		end	
	end

	def booking_till_max_6_months
		errors.add(:base, message: "Booking is allowed only for 6 months from now") if self.check_in_date.present? and self.check_out_date.present? and (self.check_in_date.to_date  > (Date.today + 6.months) or self.check_out_date.to_date  > (Date.today + 6.months))
	end
end