class Booking < ActiveRecord::Base

	belongs_to :room
	belongs_to :user

	def self.check_for_available_rooms(params)
		booked_room_ids = Booking.where("(check_in_date BETWEEN ? AND ?) OR (check_out_date BETWEEN ? AND ?) or (check_in_date <= ? AND check_out_date >= ?)", params[:check_in_date].to_date, params[:check_out_date].to_date, params[:check_in_date].to_date, params[:check_out_date].to_date, params[:check_in_date].to_date, params[:check_out_date].to_date).pluck(:room_id)
		if booked_room_ids.blank?
			if params[:category_id].present?
				available_rooms = Room.joins(:category).where('categories.id = ?', params[:category_id]) 
			else
				available_rooms = Room.all 
			end
		else
			if params[:category_id].present?
				available_rooms = Room.where('rooms.id NOT IN (?)', booked_room_ids).joins(:category).where('categories.id = ?', params[:category_id])
			else
				available_rooms = Room.where('rooms.id NOT IN (?)', booked_room_ids)
			end	
		end
		return available_rooms
	end

	def self.book_room(room_id, params)
		Booking.create(:room_id => room_id, :check_in_date => params[:check_in_date], :check_out_date => params[:check_out_date], :user_id => params[:user_id])
	end

	def self.get_booking_record(id)
		Booking.includes(:room).find_by_id(id)
	end

end
