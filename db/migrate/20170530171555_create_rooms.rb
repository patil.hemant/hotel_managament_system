class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :room_number
      t.integer :category_id
      t.float :rate

      t.timestamps null: false
    end
    add_index :rooms, :category_id
  end
end
