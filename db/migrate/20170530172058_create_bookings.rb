class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :room_id
      t.date :check_in_date
      t.date :check_out_date
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :bookings, :room_id
    add_index :bookings, :user_id
  end
end
