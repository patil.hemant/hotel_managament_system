# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create(:email => 'parity_test@gmail.com', :password => 'password')
User.create(:email => 'test_canidate@gmail.com', :password => 'password')


category1 = Category.create(:name => "Deluxe Rooms", :description => "Queen Size Bed")
category2 = Category.create(:name => "Luxury Rooms", :description => "Queen Size Bed and Pool Facing")
category3 = Category.create(:name => "Luxury Suites", :description => "King Size Bed and Pool Facing")
category4 = Category.create(:name => "Presidential Suites", :description => "King Size Bed, Pool Facing with a Gym")


# Deluxe Rooms

arr = ['A','B','C','D']
for letter in arr
	for i in 1..5
		Room.create(:room_number => "#{letter}#{0}#{i}",:category_id => category1.id, :rate => 7000)	
	end
end	

#Luxury Rooms
for letter in arr
	for i in 6..10
		Room.create(:room_number => "#{i == 10 ? letter+i.to_s : letter+0.to_s+i.to_s}",:category_id => category2.id, :rate => 8500)	
	end
end	

#Luxury Suites
for i in 11..20
	Room.create(:room_number => "D#{i}",:category_id => category3.id, :rate => 12000)	
end

for i in 1..2
	Room.create(:room_number => "E#{i}",:category_id => category3.id, :rate => 12000)	
end

#Presidential Suites

for i in 03..10
	Room.create(:room_number => "#{i == 10 ? "E"+i.to_s : "E"+0.to_s+i.to_s}",:category_id => category4.id, :rate => 20000)	
end
